/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

public class TestSimulador {
    
    @Test
    public void unitMonto2() {
        Credito2 mont2 = new Credito2(); 
        int resultado = mont2.MontoInt(130000);
	int esperado = 120000;
        assertTrue(esperado<resultado);
    }
    
    @Test
    public void unitMonto3() {
        Credito2 mont3 = new Credito2(); 
        int resultado = mont3.MontoInt(130000);
        assertNotNull(resultado);
    }
    
    @Test
    public void unitCuota2() {
        Credito2 cuot2 = new Credito2(); 
        int resultado = cuot2.CuotaInt(12);
	int esperado = 6;
        assertTrue(esperado<resultado);
    }
    @Test
    public void unitCuota3() {
        Credito2 cuot3 = new Credito2(); 
        int resultado = cuot3.CuotaInt(7);
        assertNotNull(resultado);
    }
    
    public void unitNombre() {
        Credito2 nombre = new Credito2();
	String resultado = nombre.Nombre("Juan");
        String esperado = "Juan";
	assertEquals(esperado, resultado);
    }

    @Test
    public void unitApellido() {
        Credito2 apellido = new Credito2();
	String resultado = apellido.Apellido("Silva");
        String esperado = "Silva";
	assertEquals(esperado, resultado);
    }

    @Test
    public void unitTelefono() {
        Credito2 telefono = new Credito2();
	String resultado = telefono.Telefono("958725898");
        String esperado = "958725898";
	assertEquals(esperado, resultado);
    }

    @Test
    public void unitCorreo() {
        Credito2 correo = new Credito2();
	String resultado = correo.Correo("hola@hola.com");
        String esperado = "hola@hola.com";
	assertEquals(esperado, resultado);
    }
    
    
    /*
    @Test
    public void unitMonto() {
        Credito2 monto = new Credito2(); 
        String resultado = monto.Monto("130000");
    	String esperado = "130000" ;
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void unitCuota() {
        Credito2 cuota = new Credito2();
	String resultado = cuota.Cuota("72");
        String esperado = "72";
	assertEquals(esperado, resultado);
    }
    */
}