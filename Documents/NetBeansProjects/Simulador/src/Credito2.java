import java.io.*;

public class Credito2 {
    
    public int MontoInt(int monto2){
        if (monto2>=120000 && monto2<=50000000){
            System.out.println("MONTO INGRESADO");
        }
        else{
            System.out.println("MONTO INVALIDO: Debe ser entre 120000 y 50000000");
        }
        return monto2;
    }
    
    public int CuotaInt(int cuota2){
        if (cuota2>=6 && cuota2<=72){
            System.out.println("CUOTA INGRESADA");
        }
        else{
            System.out.println("CUOTA INVALIDA: Debe ser entre 6 y 72");
        }
        return cuota2;
    }
     
    public String Nombre(String nombre){
        int i;
        int error=0;
        
        for(i=0; i<nombre.length();i++){
            if(nombre.codePointAt(i)<32){
                error=error+1;
            }
            else if(nombre.codePointAt(i)>32 && nombre.codePointAt(i)<65){
                error=error+1;
            }
            else if(nombre.codePointAt(i)>90 && nombre.codePointAt(i)<97){
                error=error+1;
            }
            else if(nombre.codePointAt(i)>122){
                error=error+1;
            }
        }
        if (error>=1){
            System.out.println("NOMBRE INVALIDO");
        }
        else{
            System.out.println("NOMBRE INGRESADO");
        }
        
        return nombre; 
    }
    
    public String Apellido(String apellido){
        int i;
        int error=0;
        
        for(i=0; i<apellido.length();i++){
            if(apellido.codePointAt(i)<32){
                error=error+1;
            }
            else if(apellido.codePointAt(i)>32 && apellido.codePointAt(i)<65){
                error=error+1;
            }
            else if(apellido.codePointAt(i)>90 && apellido.codePointAt(i)<97){
                error=error+1;
            }
            else if(apellido.codePointAt(i)>122){
                error=error+1;
            }
        }
        if (error>=1){
            System.out.println("APELLIDO INVALIDO");
        }
        else{
            System.out.println("APELLIDO INGRESADO");
        }
        
        return apellido; 
    }
    
    public String Correo(String correo){
        int i;
        int error=0;
        int punto=0;
        int arroba=0;
        
        for(i=0; i<correo.length();i++){
            if(correo.codePointAt(i)<45){
                error=error+1;
            }
            else if(correo.codePointAt(i)>46 && correo.codePointAt(i)<48){
                error=error+1;
            }
            else if(correo.codePointAt(i)>57 && correo.codePointAt(i)<64){
                error=error+1;
            }
            else if(correo.codePointAt(i)==64){
                arroba=arroba+1;
            }
            else if(correo.codePointAt(i)==46){
                punto=punto+1;
            }
            else if(correo.codePointAt(i)>90 && correo.codePointAt(i)<95){
                error=error+1;
            }
            else if(correo.codePointAt(i)==96){
                error=error+1;
            }
            else if(correo.codePointAt(i)>122){
                error=error+1;
            }
        }
        if (error>=1){
            System.out.println("CORREO INVALIDO");
        }
        else if(arroba<1 && arroba>1){
            System.out.println("CORREO INVALIDO");
        }
        else if(punto<1){
            System.out.println("CORREO INVALIDO");
        }
        else{
            System.out.println("CORREO INGRESADO");
        }
        
        return correo; 
    }
    
    public String Telefono(String telefono){
        int i;
        int error=0;
        
        for(i=0; i<telefono.length();i++){
            if(telefono.codePointAt(i)<48){
                error=error+1;
            }            
            else if (telefono.codePointAt(i)>57){
               error=error+1; 
            }
        }
        
        if (telefono.length()<9 || telefono.length()>9){
            System.out.println("DEBE INGRESAR 9 DIGITOS");
        }
        
        if (error>=1){                                 
            System.out.println("TELEFONO INVALIDO");
        }
        else{
            System.out.println("TELEFONO INGRESADO");
        }
        return telefono;
    }
    
    /*
    
    public String Monto(String monto){
        int i;
        int error=0;
        
        for(i=0; i<monto.length();i++){
            if(monto.codePointAt(i)<48){
                error=error+1;
            }            
            else if (monto.codePointAt(i)>57){
               error=error+1; 
            }
        }
        
        if (error>=1){                                 
            System.out.println("MONTO INVALIDO");
        }
        return monto;
    }
    
    public String Cuota(String cuota){
        int i;
        int error=0;
        
        for(i=0; i<cuota.length();i++){
            if(cuota.codePointAt(i)<48){
                error=error+1;
            }            
            else if (cuota.codePointAt(i)>57){
               error=error+1; 
            }
        }
        
        if (error>=1){                                 
            System.out.println("ERORR");
        }
        return cuota;
    }
    */
    
    public static void main(String[] args) throws IOException {
        String rut;
        String monto;
        String cuota;
        String fecha;
        String nombre;
        String apellido;
        String correo;
        String telefono;
        int monto2;
        int cuota2;
        
    }
    
}
